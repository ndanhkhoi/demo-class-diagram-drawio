package com.ndanhkhoi.demo2;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class Demo2Application {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = new SpringApplicationBuilder(Demo2Application.class).headless(false).run(args);
		MainFrame mainFrame = context.getBean(MainFrame.class);
		mainFrame.setVisible(true);
	}

}
