package com.ndanhkhoi.demo2;

import com.formdev.flatlaf.FlatLightLaf;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.PrintStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author khoinda
 * Created at 07:52:11 April 15, 2021
 */
@SuppressWarnings("FieldCanBeLocal")
@org.springframework.stereotype.Component
public class MainFrame extends JFrame {

    @Autowired
    private Proccessor proccessor;

    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;

    private JLabel labelChonThuMuc;
    private JLabel labelSelectedPackage;
    private JLabel labelChooseDomain;
    private JFileChooser fileDialog;
    private JTextField tfSelectedPackage;
    private JComboBox<String> cbChooseDomain;
    private JButton showFileDialogButton;
    private JButton okBtn;
    private JTextArea loggerTextArea;

    public MainFrame() {
        super("Demo2");
        FlatLightLaf.install();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(WIDTH, HEIGHT);
        this.setResizable(false);
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        this.setLayout(null);
    }

    @PostConstruct
    public void initUI() {
        loggerTextArea = new JTextArea();
        loggerTextArea.setEditable(false);
        JScrollPane loggerScrollPane = new JScrollPane(loggerTextArea, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        loggerScrollPane.setBounds(30, 175, 430, 250);
        this.add(loggerScrollPane);

        labelChonThuMuc = new JLabel("Choose folder of domain package:");
        labelChonThuMuc.setBounds(30, 15, 200, 30);
        this.add(labelChonThuMuc);
        labelSelectedPackage = new JLabel("Selected package:");
        labelSelectedPackage.setBounds(30, 55, 200, 30);
        this.add(labelSelectedPackage);
        labelChooseDomain = new JLabel("Choose domains:");
        labelChooseDomain.setBounds(30, 95, 200, 30);
        this.add(labelChooseDomain);
        fileDialog = new JFileChooser();
        fileDialog.setFileSelectionMode(1);
        tfSelectedPackage = new JFormattedTextField("Not selected yet");
        tfSelectedPackage.setBounds(150, 55, 300, 30);
        tfSelectedPackage.setEditable(false);
        tfSelectedPackage.setEnabled(false);
        this.add(tfSelectedPackage);
        cbChooseDomain = new JComboBox<>();
        cbChooseDomain.setBounds(150, 95, 300, 30);
        cbChooseDomain.setEnabled(false);
        this.add(cbChooseDomain);
        showFileDialogButton = new JButton("Open folder");
        showFileDialogButton.addActionListener((e) -> {
            int returnVal = fileDialog.showOpenDialog(this);
            if (returnVal == 0) {
                File folder = fileDialog.getSelectedFile();
                //noinspection ConstantConditions
                List<String> domains = Arrays.stream(folder.listFiles()).filter((file) -> {
                    if (file.isDirectory()) {
                        return false;
                    } else if (FilenameUtils.getExtension(file.getName()).compareToIgnoreCase("class") != 0) {
                        return false;
                    } else {
                        String fileNameWithOutExtension = FilenameUtils.removeExtension(file.getName());
                        return fileNameWithOutExtension.charAt(fileNameWithOutExtension.length() - 1) != '_';
                    }
                }).map((file) -> FilenameUtils.removeExtension(file.getName())).sorted(String::compareToIgnoreCase).collect(Collectors.toList());
                if (domains.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "No class is found !", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    cbChooseDomain.removeAllItems();
                    Objects.requireNonNull(cbChooseDomain);
                    domains.forEach(cbChooseDomain::addItem);
                    tfSelectedPackage.setText(folder.getAbsolutePath());
                    tfSelectedPackage.setEnabled(true);
                    cbChooseDomain.setEnabled(true);
                }
            }
        });
        showFileDialogButton.setVisible(true);
        showFileDialogButton.setBounds(305, 15, 150, 30);
        this.add(showFileDialogButton);
        okBtn = new JButton("OK");
        okBtn.setBounds(305, 135, 100, 30);
        okBtn.addActionListener((e) -> {
            try {
                String javaMain = "\\java\\main\\";
                int idx = tfSelectedPackage.getText().indexOf(javaMain);
                if (idx == -1) {
                    throw new RuntimeException("This folder does not supported !!");
                }

                String _package = StringUtils.replace(tfSelectedPackage.getText().substring(idx + javaMain.length()), "\\", ".");
                String mainFolder = tfSelectedPackage.getText().substring(0, idx + javaMain.length());
                URL[] urls = new URL[]{(new File(mainFolder)).toURI().toURL()};
                URLClassLoader classLoader = URLClassLoader.newInstance(urls);
                //noinspection ConstantConditions
                Class<?> clazz = classLoader.loadClass(_package + "." + cbChooseDomain.getSelectedItem().toString());
                proccessor.proccess(clazz, loggerTextArea);
            } catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
        this.add(okBtn);
        this.repaint();
    }

}
