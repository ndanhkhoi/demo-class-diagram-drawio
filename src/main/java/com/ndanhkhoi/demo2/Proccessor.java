package com.ndanhkhoi.demo2;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;

/**
 * @author khoinda
 * Created at 08:15:49 April 15, 2021
 */
@Slf4j
@Component
public class Proccessor {

    Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

    public void proccess(Class<?> clazz, JTextArea target) throws IOException {
        StringBuilder out = new StringBuilder();
        log.info("******************************************");
        ClassPathResource classPathResource = new ClassPathResource("template.xml");
        InputStream is = classPathResource.getInputStream();
        String content = IOUtils.toString(is, StandardCharsets.UTF_8);

        content = StringUtils.replace(content, "${className}", clazz.getSimpleName());
        log.info("Class: {}", clazz.getSimpleName());
        log.info("-------------------");
        out.append(clazz.getSimpleName())
            .append("\n")
            .append("------------------------")
            .append("\n");
        int fieldHeight = 20;
        StringBuilder sb = new StringBuilder();
        Field[] declaredFields = clazz.getDeclaredFields();

        for (Field declaredField : declaredFields) {
            String fieldName = declaredField.getName();
            if (!fieldName.equals("serialVersionUID")) {
                String type = declaredField.getType().getSimpleName();
                if (type.equals("Instant") || type.equals("ZonedDateTime") || type.equals("LocalTime")) {
                    type = "DateTime";
                }
                else if (type.equals("LocalDate")) {
                    type = "Date";
                }
                type = type.toLowerCase();
                sb.append(fieldName).append(": ").append(type).append("&#10;");
                log.info("{}: {}", fieldName, type);
                out.append(fieldName).append(": ").append(type).append("\n");
                fieldHeight += 16;
            }
        }

        content = StringUtils.replace(content, "${fields}", sb.toString());
        content = StringUtils.replace(content, "${fieldHeight}", fieldHeight + "");
        this.clipboard.setContents(new StringSelection(content), null);
        target.setText(out.toString());
        JOptionPane.showMessageDialog(null, "Saved to clipboard", "Success", JOptionPane.INFORMATION_MESSAGE);
    }

}
